import Vue from "vue";
import VueRouter from "vue-router";
import DashBoard from "../views/DashBoard.vue";
import Projects from "../views/Projects.vue";
import Team from "../views/Team.vue";
Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "dashboard",
        component: DashBoard,
    },
    {
        path: "/projects",
        name: "Projects",
        component: Projects,
    },
    {
        path: "/team",
        name: "DashBoard",
        component: Team,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;