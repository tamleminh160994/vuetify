import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import axios from "axios";
export default new Vuex.Store({
    state: {
        projects: [],
    },
    mutations: {
        ADD_PROJECT(state, project) {
            state.projects.push(project);
        },
        LOAD_DATAS(state, projects) {
            state.projects = projects;
        },
    },
    actions: {
        addProject({ commit }, project) {
            commit("ADD_PROJECT", project);
        },
        loadDataProjects({ commit }) {
            axios.get("http://localhost:3000/projects").then((response) => {
                commit("LOAD_DATAS", response.data);
            });
        },
    },
    modules: {},
});